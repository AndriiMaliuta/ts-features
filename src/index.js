var user = {
    name: "Daniel",
    age: 26
};
console.log(user.name); // returns undefined
var message2 = "hello!";
function greet(person, date) {
    console.log("Hello ".concat(person, ". It is ").concat(date.toLocaleDateString()));
}
