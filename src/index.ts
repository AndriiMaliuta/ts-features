const user = {
  name: "Daniel",
  age: 26,
};

console.log(user.name); // returns undefined
const message2 = "hello!";

function greet(person: string, date: Date): void {
  console.log(`Hello ${person}. It is ${date.toLocaleDateString()}`);
}

interface Person {
  name?: number;
}

let Person = {
  name: "Petro",
};

function printId(id: number | string) {
  if (typeof id === "string") {
    // In this branch, id is of type 'string'
    console.log(id.toUpperCase());
  } else {
    // Here, id is of type 'number'
    console.log(id);
  }
}
